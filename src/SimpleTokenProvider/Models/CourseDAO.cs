﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BasicAPI.Models
{
    public class CourseDAO
    {
		[Key]
		public int CourseID { get; set; }
		public string Title { get; set; }
		public int Credits { get; set; }

		public string Archivo { get; set; }
		public ICollection<EnrollmentDAO> Enrollments { get; set; }
    }
}
