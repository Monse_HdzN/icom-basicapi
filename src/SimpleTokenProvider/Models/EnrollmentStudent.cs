﻿using System;
namespace BasicAPI.Models
{
    public class EnrollmentStudent
    {
		public int EnrollmentID { get; set; }
		public Student Student { get; set; }
    }
}
