﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace BasicAPI.Models
{
    public class StudentDAO
    {
		[Key]
		public int StudentID { get; set; }
		public string LastName { get; set; }
		public string FirstMidName { get; set; }
		public DateTime EnrollmentDate { get; set; }
		public ICollection<EnrollmentDAO> Enrollments { get; set; }
    }
}
