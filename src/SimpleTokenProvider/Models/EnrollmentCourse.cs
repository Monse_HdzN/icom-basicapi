﻿using System;
namespace BasicAPI.Models
{
    public class EnrollmentCourse
    {
        public int EnrollmentID { get; set; }
        public int CourseID { get; set; }
        public Course Course { get; set; }
    }
}
