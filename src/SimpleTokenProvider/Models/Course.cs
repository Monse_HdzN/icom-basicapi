﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

namespace BasicAPI.Models
{
	public class Course
	{
        
		public int CourseID { get; set; }
		public string Title { get; set; }
		public int Credits { get; set; }
        public string Archivo { get; set; }
		
	}
}