﻿using System;
using BasicAPI.Models;
using System.ComponentModel.DataAnnotations;
namespace BasicAPI.Models
{
    public class EnrollmentDAO
    {
        [Key]
        public int EnrollmentID { get; set; }
        public int CourseID { get; set; }
        public int StudentID { get; set; }
        public CourseDAO Course { get; set; }
        public StudentDAO Student { get; set; }
    }
}
