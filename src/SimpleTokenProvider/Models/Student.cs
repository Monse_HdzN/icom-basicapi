﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

namespace BasicAPI.Models
{
	public class Student
	{
       
		public int StudentID { get; set; }
		public string LastName { get; set; }
		public string FirstMidName { get; set; }
		public DateTime EnrollmentDate { get; set; }
		/* //JSON
		 [JSONIgnore]

		 //Ejecucion
		 [DataMember(EmitDefault = false)]

		 //BD
		 [NotMapped]*/
		
	}
}