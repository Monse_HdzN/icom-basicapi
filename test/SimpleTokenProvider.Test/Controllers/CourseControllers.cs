﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using BasicAPI.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using BasicAPI.API.Helpers;
using BasicAPI.API.Models;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace BasicAPI.API.Controllers
{
	[Route("api/[controller]")]
	public class CoursesController : Controller
	{
		private readonly SchoolContext _context;
		private readonly MapperConfiguration mapperConfig;
		private readonly IMapper mapper;
		private readonly MapperConfiguration mapperConfigEnrollments;
		private readonly IMapper mapperenrollments;
		private readonly MapperConfiguration mapperConfig2;
		private readonly IMapper mapper2;
		public CoursesController(SchoolContext context)
		{
			_context = context;
			this.mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<CourseDAO, Course>());
			this.mapper = mapperConfig.CreateMapper();
			this.mapperConfigEnrollments = new MapperConfiguration(cfg =>
			{
                cfg.CreateMap<EnrollmentDAO, EnrollmentStudent>();
                cfg.CreateMap<StudentDAO, Student>();
			});
			this.mapperenrollments = mapperConfigEnrollments.CreateMapper();
			this.mapperConfig2 = new MapperConfiguration(cfg => cfg.CreateMap<Course, CourseDAO>());
			this.mapper2 = mapperConfig2.CreateMapper();
		}

		[HttpGet]
		//[Authorize]
		public async Task<IActionResult> GetAll()
		{
            var curso = await _context.Courses.ToListAsync();

			List<Course> dto = this.mapper.Map<List<Course>>(curso);
			return new ObjectResult(dto);
		}

       
		[HttpGet("{id}", Name = "GetCourses")]
		public async Task<IActionResult> Details(int id)
		{
			//int id = 1;
			var course = await _context.Courses.SingleOrDefaultAsync(m => m.CourseID == id);

			if (course == null)
			{
				return NotFound();
			}
            Course dto = this.mapper.Map<Course>(course);
			return new ObjectResult(course);
		}

		[HttpGet("{id}/students", Name = "GetCoursesStudents")]
		public async Task<IActionResult> DetailsStudents(int id)
		{
			try
			{
				var course = await _context.Enrollments.
											Include(s => s.Student)
											//.ThenInclude(e => e.Course)
                                            .Where(e => e.CourseID == id)
											.ToListAsync();
				if (course == null)
				{
					return NotFound();
				}
                List<EnrollmentStudent> dto = this.mapperenrollments.Map<List<EnrollmentStudent>>(course);

				return new OkObjectResult(dto);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}

			return new OkResult();
		}

		[HttpPost]
		//[Authorize]
		public async Task<IActionResult> Create([FromBody] Course course)
		{
			try
			{
				if (!ModelState.IsValid)
				{

					return BadRequest();
				}
                course.Archivo = null;
				CourseDAO dao = this.mapper2.Map<CourseDAO>(course);

				_context.Add(dao);
				await _context.SaveChangesAsync();
                Course dto = this.mapper.Map<Course>(dao);
                return new ObjectResult(dto);
			}
			catch (DbUpdateException ex)
			{

				//Log the error (uncomment ex variable name and write a log.
				ModelState.AddModelError("", "Unable to save changes. " +
										 "Try again, and if the problem persists " +
										 "see your system administrator.");
				return BadRequest(ex.Message);
			}
           
		}

		//[HttpPost("{id}/upload", Name = "GetCourses")]
		[HttpPost("{id}/upload", Name = "PostCourseFile")]
        [Authorize]
		public async Task<IActionResult> UploadFile(int id, IFormFile file)
		{
			try
			{
				var curso = await _context.Courses.SingleOrDefaultAsync(m => m.CourseID == id);
				if (curso == null)
				{
					return NotFound();
				}

				StorageConnection connection = new StorageConnection();
				string archivo = await connection.Upload(file, Guid.NewGuid().ToString(), "courses");

				curso.Archivo = archivo;

				_context.Courses.Update(curso);
				_context.SaveChanges();

			}
			catch (Exception ex)
			{

				//Log the error (uncomment ex variable name and write a log.
				ModelState.AddModelError("", "Unable to save changes. " +
										 "Try again, and if the problem persists " +
										 "see your system administrator.");
				return BadRequest(ex.Message);
			}
			return new OkResult();
		}


		[HttpPut("{id}")]
        //[Authorize]
		public IActionResult Update(int id, [FromBody]  Course course)
		{
            CourseDAO dao = this.mapper2.Map<CourseDAO>(course);
			if (dao == null || dao.CourseID != id)
			{
				return BadRequest();
			}
			var curso = _context.Courses.FirstOrDefault(t => t.CourseID == id);
			if (curso == null)
			{
				return NotFound();
			}

			curso.Credits = dao.Credits;
			curso.Title = dao.Title;

			_context.Courses.Update(curso);
			_context.SaveChanges();
            Course dto = this.mapper.Map<Course>(course);
			return new OkObjectResult(dto);

		}
		[HttpDelete("{id}")]
       // [Authorize]
		public IActionResult Delete(int id)
		{
			var curso = _context.Courses.First(t => t.CourseID == id);
			if (curso == null)
			{
				return NotFound();
			}

			_context.Courses.Remove(curso);
			_context.SaveChanges();
			return new OkResult();


		}

	}
}