﻿using System;
using BasicAPI.API.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using BasicAPI.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace BasicAPI.API.Controllers
{
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {
        private readonly SchoolContext _context;
        private readonly MapperConfiguration mapperConfig;
        private readonly IMapper mapper;
        private readonly MapperConfiguration mapperConfigEnrollments;
        private readonly IMapper mapperenrollments;
        private readonly MapperConfiguration mapperConfig2;
        private readonly IMapper mapper2;

        public StudentsController(SchoolContext context)
        {
            _context = context;
            this.mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<StudentDAO, Student>());
            this.mapper = mapperConfig.CreateMapper();
            this.mapperConfigEnrollments = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EnrollmentDAO, EnrollmentCourse>();
                cfg.CreateMap<CourseDAO, Course>();
            });
            this.mapperenrollments = mapperConfigEnrollments.CreateMapper();
            this.mapperConfig2 = new MapperConfiguration(cfg => cfg.CreateMap<Student, StudentDAO>());
            this.mapper2 = mapperConfig2.CreateMapper();


        }


        /* [HttpGet]
         [Authorize]
         public IEnumerable<Student> GetAll()
         {
             return _context.Students.ToList();
         }*/

        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll()
        {
            var estudiante = await _context.Students.ToListAsync();

            List<Student> dto = this.mapper.Map<List<Student>>(estudiante);
            return new ObjectResult(dto);
        }

        
        [HttpGet("{id}", Name = "GetStudents")]
        //[Authorize]
        public async Task<IActionResult> Details(int id)
        {
            //int id = 1;
            var estudiante = await _context.Students.SingleOrDefaultAsync(m => m.StudentID == id);
           

            if (estudiante == null)
            {
                return NotFound();
            }

            Student dto = this.mapper.Map<Student>(estudiante);
            return new ObjectResult(dto);
        }

       
        [HttpGet("{id}/enrollments", Name = "GetStudentsEnrollments")]
        //[Authorize]
        public async Task<IActionResult> DetailsEnrollments(int id)
        {
            try
            {
                var student = await _context.Enrollments.
                                            Include(s => s.Course)
                                            //.ThenInclude(e => e.Course)
                                            .Where(e => e.StudentID == id)
                                            .ToListAsync();
                if (student == null)
                {
                    return NotFound();
                }
                List<EnrollmentCourse> dto = this.mapperenrollments.Map<List<EnrollmentCourse>>(student);
              
                return new OkObjectResult(dto);  
            }catch(Exception e){
                Console.WriteLine(e);
            }

            return new OkResult();
        }


        //[Route("api/[controller]/add")]
        [HttpPost]
        //[Authorize]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody]  Student student)
        {
            try
            {
                if (!ModelState.IsValid)
                {

                    return BadRequest();
                }
                StudentDAO dao = this.mapper2.Map<StudentDAO>(student);
                _context.Add(dao);
                await _context.SaveChangesAsync();
                Student dto = this.mapper.Map<Student>(dao);
                return new ObjectResult(dto);

                //Validar que el contexto autoincremente el Id y el objeto student esté actualizado
                // No, hacer query para extraer student

                //Si
                //Enrollment a = new Enrollment();
                //a.StudentID = student.StudentID;
                //_context.Add(a);
                //await _context.SaveChangesAsync();

                //Mapear studentdao a studentDto
                //studentDTO.Enrollments = //Resultado de query

                //return OkObjectResult(studentDTO
            }
            catch (DbUpdateException ex)
            {

                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                                         "Try again, and if the problem persists " +
                                         "see your system administrator.");
                return BadRequest(ex.Message);
            }

        }

        [HttpPut("{id}")]
        //[Authorize]
        public IActionResult Update(int id, [FromBody]  Student student)
        {
            StudentDAO dao = this.mapper2.Map<StudentDAO>(student);
            if (dao == null || dao.StudentID != id)
            {
                return BadRequest();
            }
            var estudiante = _context.Students.FirstOrDefault(t => t.StudentID == id);
            if (estudiante == null)
            {
                return NotFound();
            }

             

            estudiante.EnrollmentDate = dao.EnrollmentDate;
            estudiante.FirstMidName =dao.FirstMidName;
            estudiante.LastName = dao.LastName;



            _context.Students.Update(estudiante);
            _context.SaveChanges();
            Student dto = this.mapper.Map<Student>(estudiante);
            return new OkObjectResult(dto);



        }
        /*public async Task<IActionResult> EditPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var studentToUpdate = await _context.Students.SingleOrDefaultAsync(s => s.StudentID == id);
            if (await TryUpdateModelAsync<Student>(
                studentToUpdate,
                "",
                s => s.FirstMidName, s => s.LastName, s => s.EnrollmentDate))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateException /* ex *///)
                /*{
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(studentToUpdate);
        }*/
        [HttpDelete("{id}")]
        //[Authorize]
        public IActionResult Delete(int id)
        {
            var estudiante = _context.Students.First(t => t.StudentID == id);
            if (estudiante == null)
            {
                return NotFound();
            }

            _context.Students.Remove(estudiante);
            _context.SaveChanges();
            return new OkResult();


        }
    }
}