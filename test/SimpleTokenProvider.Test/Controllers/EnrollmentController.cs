﻿using System;

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using BasicAPI.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BasicAPI.API.Models;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace BasicAPI.API.Controllers
{
    [Route("api/[controller]")]
    public class EnrollmentsController : Controller
    {
        private readonly SchoolContext _context;
        private readonly MapperConfiguration mapperConfig;
        private readonly IMapper mapper;
        private readonly MapperConfiguration mapperConfig2;
        private readonly IMapper mapper2;


        public EnrollmentsController(SchoolContext context)
        {
            _context = context;
            this.mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<EnrollmentDAO, Enrollment>());
            this.mapper = mapperConfig.CreateMapper();
            this.mapperConfig2 = new MapperConfiguration(cfg => cfg.CreateMap<Enrollment,EnrollmentDAO>());
            this.mapper2 = mapperConfig2.CreateMapper();

           
        }


        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetAll()
        {
            var inscripcion = await _context.Enrollments.ToListAsync();

            List<Enrollment> dto = this.mapper.Map<List<Enrollment>>(inscripcion);
            return new ObjectResult(dto);
        }


        [HttpGet("{id}", Name = "GetEnrollments")]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                //int id = 1;
                var inscripcion = await _context.Enrollments.SingleOrDefaultAsync(m => m.EnrollmentID == id);

                if (inscripcion == null)
                    return NotFound();

                Enrollment dto = this.mapper.Map<Enrollment>(inscripcion);
                return new ObjectResult(dto);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return StatusCode(409);
        }


        [HttpPost]
        //[Authorize]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody]  Enrollment enrollment)
        {
            try
            {
                if (!ModelState.IsValid)
                {

                    return BadRequest();
                }

                EnrollmentDAO dao = this.mapper2.Map<EnrollmentDAO>(enrollment);

                var estudiante = await _context.Students.FirstAsync(m => m.StudentID.Equals(dao.StudentID));
                var curso = await _context.Courses.FirstAsync(t => t.CourseID.Equals(dao.CourseID));
                if (estudiante != null && curso != null)
                {
                    
                    _context.Add(dao);
                    await _context.SaveChangesAsync();
                    Enrollment dto = this.mapper.Map<Enrollment>(dao);
                    return new ObjectResult(dto);
                }


            }
            catch (DbUpdateException ex)
            {

                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                                         "Try again, and if the problem persists " +
                                         "see your system administrator.");
                return BadRequest(ex.Message);
            }
            catch (Exception )
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                                         "Try again, and if the problem persists " +
                                         "see your system administrator.");
                return StatusCode(409);
            }
            return new OkResult();

        }

        [HttpPut("{id}")]
        [Authorize]
        public IActionResult Update(int id, [FromBody] Enrollment enrollment)
        {
            try
            {

				EnrollmentDAO dao = this.mapper2.Map<EnrollmentDAO>(enrollment);
                if (dao == null || dao.EnrollmentID != id)
                {
                    return BadRequest();
                }

                var inscripcion = _context.Enrollments.FirstOrDefault(t => t.EnrollmentID == id);
                if (inscripcion == null)
                {
                    return NotFound();
                }

                var estudiante = _context.Students.First(m => m.StudentID.Equals(dao.StudentID));
                var curso = _context.Courses.First(t => t.CourseID.Equals(dao.CourseID));
                if (estudiante != null && curso != null)
                {

                    inscripcion.StudentID = dao.StudentID;
                    inscripcion.CourseID = dao.CourseID;

                    _context.Enrollments.Update(inscripcion);
                    _context.SaveChanges();


                }
                return new OkObjectResult(enrollment);
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.Message);

            }
            catch (Exception)
            {
                return StatusCode(409);
            }

        }
        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            var inscripcion = _context.Enrollments.First(t => t.EnrollmentID == id);
            if (inscripcion == null)
            {
                return NotFound();
            }

            _context.Enrollments.Remove(inscripcion);
            _context.SaveChanges();
            return new OkResult();


        }
    }
}