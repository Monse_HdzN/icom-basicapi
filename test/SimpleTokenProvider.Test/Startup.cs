using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

using BasicAPI.API.Models;

namespace BasicAPI.API
{
    public partial class Startup
  {
    public Startup(IHostingEnvironment env)
    {
            /*
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();
      Configuration = builder.Build();
      */
    }

    public IConfigurationRoot Configuration { get; set; }

    public void ConfigureServices(IServiceCollection services)
    {
	  var connection = @"Server=tcp:icom-universityserver.database.windows.net,1433;Initial Catalog=icom-university;Persist Security Info=False;User ID=icom-monse;Password=qzwxec123$%;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
	  services.AddDbContext<SchoolContext>(options => options.UseSqlServer(connection));
      services.AddMvc();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      loggerFactory.AddConsole(LogLevel.Debug);
      loggerFactory.AddDebug();

      ConfigureAuth(app);

      app.UseStaticFiles();

      app.UseMvc();
    }
  }
}