﻿using BasicAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace BasicAPI.API.Models
{
	public class SchoolContext : DbContext
	{
		public SchoolContext(DbContextOptions<SchoolContext> options) : base(options)
		{
		}

		public DbSet<CourseDAO> Courses { get; set; }
		public DbSet<EnrollmentDAO> Enrollments { get; set; }
		public DbSet<StudentDAO> Students { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<CourseDAO>().ToTable("Course");
			modelBuilder.Entity<EnrollmentDAO>().ToTable("Enrollment");
			modelBuilder.Entity<StudentDAO>().ToTable("Student");
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(@"Server=tcp:icom-universityserver.database.windows.net,1433;Initial Catalog=icom-university;Persist Security Info=False;User ID=icom-monse;Password=qzwxec123$%;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
		}
	}
}
