﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace BasicAPI.API.Helpers
{
	public class StorageConnection
	{
		private StorageCredentials storageCredentials;
		private CloudStorageAccount cloudStorageAccount;

		public StorageConnection()
		{
			this.storageCredentials = new StorageCredentials("icomstorage", "GVLwdQOaGlbKLHyD1Ob7yIjj0/HO7PL3LToHkBXdcfUwPkPJP8yS7jSGUBpKus9u+V5bL3oU3RteZbiZKrimTg==");
			this.cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
		}

		public async Task<string> Upload(IFormFile file, string fileName, string nameContainer)
		{
			var cloudBlobClient = this.cloudStorageAccount.CreateCloudBlobClient();
			var container = cloudBlobClient.GetContainerReference(nameContainer);
			await container.CreateIfNotExistsAsync();

			CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

			// Create or overwrite the blob with the contents of a local 
			//string file = "/Users/becario1/Desktop/adios.txt";
			using (var fileStream = file.OpenReadStream()) // file.OpenReadStream())
			{
				await blockBlob.UploadFromStreamAsync(fileStream);
			}

			return blockBlob.Uri.AbsoluteUri;
		}
	}
}
